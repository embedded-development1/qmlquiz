import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Window {
    width: 400
    height: 300
    visible: true
    title: qsTr("Quiz Game")
    Rectangle {
        width: parent.width
        height: parent.height
        anchors.fill: parent
        color: "white"
        border.color: "black"
        border.width: 5
        radius: 2
    }
    ColumnLayout{
        id:main
        anchors.fill: parent
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10
        RowLayout{
            id:info
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle{
                width: info.width
                height: info.height
                color: "lightgray"
            }
            Label{
                id:usernametext
                text: "Username: "
                font.pixelSize:  12
                anchors.top: info.top
                anchors.bottom: info.bottom
                anchors.left: info.left
            }
            Item{
                id:extenderinfo
                Layout.fillWidth: true
                anchors.top: info.top
                anchors.bottom: info.bottom
                anchors.left: usernametext.right
            }
            Label{
                text: "Score: 0"
                font.pixelSize:  12
                anchors.top: info.top
                anchors.bottom: info.bottom
                anchors.right: info.right
            }
        }
        ColumnLayout{
            id:questions
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle{
                width: questions.width
                height: questions.height
                color: "lightgray"
            }
            Label{
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "Question here"
                font.pixelSize:  12
                anchors.top: questions.top
                anchors.bottom: questions.bottom
                anchors.left: questions.left
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
        GridLayout{
            id:answerbutton
            Layout.fillWidth: true
            Layout.fillHeight: true
            columns:2
            rows:2

            Rectangle{
                width: answerbutton.width
                height: answerbutton.height
                color: "lightgray"
            }
            Button{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.row: 0
                Layout.column: 0
                text: "Answer1"
            }
            Button{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.row: 1
                Layout.column: 0
                text: "Answer2"
            }
            Button{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.row: 0
                Layout.column: 1
                text: "Answer3"
            }
            Button{
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.row: 1
                Layout.column: 1
                text: "Answer4"
            }
        }
        RowLayout{
            id:result
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle{
                width: result.width
                height: result.height
                color: "lightgray"
            }
            Item{
                Layout.fillWidth: true
            }
            Label{
                text: "Answer"
                font.pixelSize:  12
            }
            Item{
                Layout.fillWidth: true
            }
        }
        ColumnLayout{
            id:progress
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle{
                width: progress.width
                height: progress.height
                color: "lightgray"
            }

            RowLayout{
                id:timer
                Layout.fillWidth: true
                Layout.fillHeight: true

                Rectangle{
                    width: timer.width
                    height: timer.height
                    color: "lightgray"
                }
                Item{
                    Layout.fillWidth: true
                }
                Label{
                    text: "Timer"
                    font.pixelSize:  12
                }
                Item{
                    Layout.fillWidth: true
                }
            }
            ProgressBar{
                value: 0.2
                Layout.fillWidth: true
            }
        }
    }

}
